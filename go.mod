module api_cv

go 1.15

require (
	github.com/Bowery/prompt v0.0.0-20190916142128-fa8279994f75 // indirect
	github.com/codegangsta/negroni v1.0.0
	github.com/dchest/safefile v0.0.0-20151022103144-855e8d98f185 // indirect
	github.com/eclipse/paho.mqtt.golang v1.3.2
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/kardianos/govendor v1.0.9 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/nabeken/negroni-auth v0.0.0-20170421074600-c65ff4ad85ca
	github.com/onsi/ginkgo v1.15.1 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/cors v1.7.0
	github.com/silkeh/senml v0.0.0-20210225180226-d67cf039d26c
	github.com/urfave/negroni v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/sys v0.0.0-20210326220804-49726bf1d181 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/redis.v3 v3.6.4
)
