package redis

import (
	"gopkg.in/redis.v3"
	"time"
)
const (
	DBMcuInfo    = 2 //m-gateway info
	DBMedia      = 3 //media info
	DBLiveStream = 1 //livestream info
)
var (

	client  *redis.Client
	iClient *redis.Client
	mClient *redis.Client
	lClient *redis.Client
)
func IsInit() bool {
	return client != nil
}
func Ping() bool {
	_, err := client.Ping().Result()
	return err == nil
}
func Init(address string, password string) {
	if client == nil {
		client = redis.NewClient(&redis.Options{
			Network:  "tcp",
			Addr:     address,
			Password: password,
			//WriteTimeout: time.Second * 2,
			IdleTimeout: time.Minute * 3,
			DB:          int64(0),
			PoolSize:    10000,
			PoolTimeout: time.Minute * 5,
			//MaxRetries:   3,
		})
		lClient = redis.NewClient(&redis.Options{
			Network:  "tcp",
			Addr:     address,
			Password: password,
			//WriteTimeout: time.Second * 2,
			IdleTimeout: time.Minute * 4,
			DB:          int64(DBLiveStream),
			PoolSize:    10000,
			PoolTimeout: time.Minute * 5,
			//MaxRetries:   3,
		})
		iClient = redis.NewClient(&redis.Options{
			Network:  "tcp",
			Addr:     address,
			Password: password,
			//WriteTimeout: time.Second * 2,
			IdleTimeout: time.Minute * 4,
			DB:          int64(DBMcuInfo),
			PoolSize:    10000,
			PoolTimeout: time.Minute * 5,
			//MaxRetries:   3,
		})
		mClient = redis.NewClient(&redis.Options{
			Network:  "tcp",
			Addr:     address,
			Password: password,
			//WriteTimeout: time.Second * 2,
			IdleTimeout: time.Minute * 4,
			DB:          int64(DBMedia),
			PoolSize:    10000,
			PoolTimeout: time.Minute * 5,
			//MaxRetries:   3,
		})
	}
}
func Init1(address ... string) {
	if client == nil {
		client = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName: "mymaster",
			SentinelAddrs: []string{
				address[0],
				address[1],
				address[2],
			},
		})
		lClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName: "mymaster",
			SentinelAddrs: []string{
				address[0],
				address[1],
				address[2],
			},
		})
		iClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName: "mymaster",
			SentinelAddrs: []string{
				address[0],
				address[1],
				address[2],
			},
		})
		mClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName: "mymaster",
			SentinelAddrs: []string{
				address[0],
				address[1],
				address[2],
			},
		})
	}
}