package main

import (
	"api_cv/core_api/redis"
	"api_cv/core_api/settingsMCU"
	"api_cv/mcu_api/routers"
	"api_cv/mcu_api/services_mcu"
	"flag"
	"fmt"
	"github.com/codegangsta/negroni"
	"github.com/golang/glog"
	"github.com/rs/cors"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)
const version string = "dev_0_1"
func init() {
	//glog
	//create logs folder
	absPath, _ := filepath.Abs(filepath.Join("mcu_api","logs"))
	fmt.Println(absPath)
	err := os.Mkdir(absPath, 0777)
	if err !=nil{
		fmt.Println("Folder Logs MCU already exist, Continue writing log to this folder")
	}
	flag.Lookup("stderrthreshold").Value.Set("[INFO|WARN|FATAL]")
	flag.Lookup("logtostderr").Value.Set("false")
	flag.Lookup("alsologtostderr").Value.Set("true")
	flag.Lookup("log_dir").Value.Set(absPath)
	glog.MaxSize = 1024 * 1024 * settingsMCU.GetGlogConfig().MaxSize
	flag.Lookup("v").Value.Set(fmt.Sprintf("%d", settingsMCU.GetGlogConfig().V))
	flag.Parse()
}
func main()  {
	flag.Parse()
	glog.Infof("START LP-MCU-API %v, at: %v", version, time.Now())
	glog.Info("Init Redis database...")
	//Tạo kết nối đến redis database
	redis.Init1(settingsMCU.GetRedisInfo().Host1,settingsMCU.GetRedisInfo().Host2,settingsMCU.GetRedisInfo().Host3)
	//redis.Init("0.0.0.0:6379","mypass")
	if !redis.Ping() {
		glog.Info("redis ping error")
		os.Exit(0)
	} else {
		fmt.Println("OK Redis")
		glog.Info("OK")
	}
	//Kết nối đến MQTT,
	//User và Password sẽ được lưu vào redis
	err := services_mcu.SetSuperUser()
	if err == nil {
		glog.Info("Connecting to MQTT broker... ")
		err = services_mcu.ConnectMqtt()
		if err != nil {
			fmt.Println("Fail to connect to MQTT Broker")
			glog.Error("Failed err: ", err)
			os.Exit(1)
		} else {
			fmt.Println("OK MQTT Connect")
			glog.Info("OK")
		}
	}
	routerApi := routers.InitRoutes()
	nApi := negroni.Classic()
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"DELETE", "PUT", "GET", "HEAD", "OPTIONS", "POST"},
	})
	nApi.Use(c)
	nApi.UseHandler(routerApi)
	listenTo := settingsMCU.GetRestfulApiHost()+":" + strconv.Itoa(settingsMCU.GetRestfulApiPort())
	fmt.Println(listenTo)
	go http.ListenAndServe(listenTo,nApi)
	stats()
}
func stats() {
	var s string
	for {
		fmt.Scanln(&s)
		switch s {
		case "q":
			services_mcu.DisconnectMqtt()
			glog.Info("exited")
			os.Exit(0)
		case "clientid":
			services_mcu.LoadClientIdToRedis()
		}
	}
}