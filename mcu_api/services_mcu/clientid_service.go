package services_mcu

import (
	"api_cv/core_api/redis"
	"bufio"
	"bytes"
	"fmt"
	"github.com/golang/glog"
	"io"
	"net/http"
	"os"
)
const FILE_NAME string = "clientids.txt"
func LoadClientIdToRedis() {
	lines, err := readLines(FILE_NAME)
	if err != nil {
		glog.Error("LoadClientIdToRedis/readLines err: ", err)
		return
	}
	success := 0
	failure := 0
	for _, line := range lines {
		//fmt.Println(line)
		err := redis.UpdateMcuClientId(line)
		if err != nil {
			failure = failure + 1
			glog.Error("LoadClientIdToRedis/redis.UpdateMcuClientId err: ", err)
		} else {
			success = success + 1
		}
	}
	glog.Infof("Load clientid to redis: %d --->success; %d --->failure", success, failure)
}
func SetClientIds(lines []string) (int, []byte) {
	success := 0
	failure := 0
	for _, line := range lines {
		err := redis.UpdateMcuClientId(line)
		if err != nil {
			failure = failure + 1
			glog.Error("SetClientIds/redis.UpdateMcuClientId err: ", err)
		} else {
			success = success + 1
		}
	}
	res := []byte(fmt.Sprintf(`{"success": %d, "failure": %d}`, success, failure))
	return http.StatusOK, res
}
// Read a whole file into the memory and store it as array of lines
func readLines(path string) ([]string, error) {
	var (
		part   []byte
		prefix bool
	)
	lines := []string{}
	file, err := os.Open(path)
	if err != nil {
		return lines, err
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 0))
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	if err == io.EOF {
		err = nil
	}
	return lines, err
}