package services_mcu

import (
	"api_cv/core_api/base"
	"api_cv/core_api/models"
	"api_cv/core_api/redis"
	"api_cv/core_api/settingsMCU"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"hash/crc32"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)
const TIME_ZONE = 7 * 3600
func PublishNewRecord3(record *models.Record) (int, []byte) {
	//time unix
	now := time.Now()
	loc := now.Location()
	fmt.Println(loc)
	timeStartUnix := time.Date(record.RecPlayStart.Year(), record.RecPlayStart.Month(), record.RecPlayStart.Day(), 0, 0, 0, 0, loc).Unix()
	timeExpiredUnix := time.Date(record.RecPlayExpire.Year(), record.RecPlayExpire.Month(), record.RecPlayExpire.Day(), 23, 59, 59, 0, loc).Unix()
	timeCreateUnix := now.Unix()
	glog.V(8).Info(mediaInfoToString(record))
	//TODO: dowload file from http api
	//urlSafe := url.QueryEscape(*record.RecAudio)
	urlSafe := (*record.RecAudio)
	file := fmt.Sprintf("%s/%s", settingsMCU.GetFileServerPath(), urlSafe)
	//file = "https://s103.123apps.com/acutter/d/s103J81OpsPk_mp3_1LCycaDR.wav"
	//file = urlSafe
	fmt.Println(*record.RecAudio)
	fmt.Println("ALOO ALOO")
	//fmt.Println(settingsMCU.GetFileServerPath())
	//file = "https://s104.123apps.com/acutter/d/s1044RMnqek3_mp3_HZuaHYMz.wav"
	filePublic := fmt.Sprintf("%s/%s", settingsMCU.GetFileServerPublicPath(), urlSafe)
	//filePublic = "https://s103.123apps.com/acutter/d/s103J81OpsPk_mp3_1LCycaDR.wav"
	//filePublic = urlSafe
	glog.V(8).Info(fmt.Sprintf("Downloading from %s", file))
	bytes, err := downloadMedia(file)
	//bytes, err := downloadMedia("https://c1-ex-swe.nixcdn.com/NhacCuaTui1004/TheThai-HuongLy-6728509.mp3?st=txtTCY0ah5EUj15-Ms4Bmg&e=1615863175&download=true")
	//bytes, err := openMedia(file)
	if err != nil {
		fmt.Println("IN11")
		glog.Errorf("PublishNewRecord2/downloadMedia from: %s err: %v", file, err)
		return http.StatusInternalServerError, nil
	}
	size := len(bytes)
	checksum := crc32.ChecksumIEEE(bytes)
	glog.V(8).Info(size, " bytes downloaded")

	mediaData := &base.OPHMediaData2{
		MediaId:       *record.RecId,
		Header:        int(record.RecHeader),
		Summary:       *record.RecSummary,
		Priority:      byte(record.RecPriority),
		CreateTime:    timeCreateUnix,
		StartTime:     timeStartUnix,
		ExpireTime:    timeExpiredUnix,
		RepeatType:    byte(record.RecPlayRepeatType),
		RepeatDays:    int32(record.RecPlayRepeatDays),
		PlayMode:      byte(record.RecPlayMode),
		PlayTime:      record.RecPlayTime,
		AudioSize:     int64(size),
		AudioCodec:    base.CODEC_G711_ALAW,
		AudioFormat:   base.AUDIO_FORMAT_WAV,
		AudioChecksum: checksum,
		Url:           filePublic,
	}
	mediaDataJson, _ := json.Marshal(mediaData)
	fmt.Println(string(mediaDataJson))
	//publish media data
	for _, id := range *record.RecReceiptIds {
		var toId int64 = 0
	//	topic := fmt.Sprintf(groupTopicFormat, id)
		topic2 := fmt.Sprintf(groupTopicFormat2, id)
		if record.RecReceiptType == int32(base.RECEIPT_TYPE_MCU) {
			fmt.Println("IN type mcu")
			toId = id
			//topic = fmt.Sprintf(mcuTopicFormat, id)
			topic2 = fmt.Sprintf(mcuTopicFormat2, id)
		}
		//push v2
		msg := &base.MqttMessagePublish{
			Id:     toId,
			OpCode: base.OPH_MEDIA_EDIT_3,
			Data:   json.RawMessage(mediaDataJson),
		}
		fmt.Println("Pub to topic2: ", topic2)
		err := publishMessage(topic2, 2, false, msg)
		if err != nil {
			fmt.Println("IN")
			glog.Errorf("PublishNewRecord2/publishMessage to %s err: %v", topic2, err)
			return http.StatusInternalServerError, nil
		}
		glog.V(8).Infof("PublishNewRecord2/MediaData %s ----> success", topic2)
	}

	//Update to Redis
	err = redis.UpdateRecordInfo(record)
	if err != nil {
		glog.Error("PublishNewRecord2/redis.UpdaterecordInfo err: ", err)
	}
	res := &models.RecordRes{
		RecId: *record.RecId,
	}
	json, _ := json.Marshal(&res)
	return http.StatusOK, json
}
func mediaInfoToString(record *models.Record) string {
	mode := "do_not_play"
	if record.RecPlayMode == base.PLAY_MODE_SCHEDULE {
		mode = "schedule"
	} else if record.RecPlayMode == base.PLAY_MODE_IMMEDIATELY {
		mode = "immediately"
	}

	codec := "PCM/16bit/8kHz"
	if record.RecAudioCodec == base.CODEC_G711_ALAW {
		codec = "G.711/A-law"
	} else if record.RecAudioCodec == base.CODEC_G726 {
		codec = "G.726"
	} else if record.RecAudioCodec == base.CODEC_OPUS {
		codec = "Opus"
	}
	timeStart := record.RecPlayStart.Format("2006-01-02")
	timeExpired := record.RecPlayExpire.Format("2006-01-02")
	repeat := "week"
	if record.RecPlayRepeatType == base.PLAY_REAPEAT_MONTH {
		repeat = "month"
	}
	receipt := "mcus"
	if record.RecReceiptType == base.RECEIPT_TYPE_GROUP {
		receipt = "groups"
	}
	return fmt.Sprintf("Summary: %s; Codec: %s; Mode: %s; TimeList: %s; Priority: %d; TStart: %s; TExpried: %s; Repeat: %s - %s; To: %s - %s", *record.RecSummary, codec, mode, timesToString(record.RecPlayTime), record.RecPriority, timeStart, timeExpired, repeat, intToDays(record.RecPlayRepeatType, record.RecPlayRepeatDays), receipt, arrayInt64ToString(*record.RecReceiptIds))
}
func downloadMedia(fileUrl string) ([]byte, error) {
	response, err := http.Get(fileUrl)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode == http.StatusOK {
		bytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, err
		}
		return bytes, err
	} else {
		return nil, errors.New(fmt.Sprintf("Status code: %d", response.StatusCode))
	}
}
func intToDays(_type, days int32) string {
	result := "--"
	arr := []string{}
	if _type == 0 {
		for i := 0; i < 8; i++ {
			if (byte(days) & (1 << byte(i))) != byte(0) {
				w := i + 2
				t := "CN"
				if w < 8 {
					t = "T" + strconv.Itoa(w)
				}
				arr = append(arr, t)
			}
		}
		result = strings.Join(arr, ", ")
	} else {
		for i := 0; i < 31; i++ {
			if (uint32(days) & (1 << uint32(i))) != uint32(0) {
				d := i + 1
				t := "D" + strconv.Itoa(d)
				arr = append(arr, t)
			}
		}
		result = strings.Join(arr, ", ")
	}
	return result
}
func arrayInt64ToString(arr []int64) string {
	result := "--"
	list := []string{}
	for _, i := range arr {
		list = append(list, strconv.FormatInt(i, 10))
	}
	result = strings.Join(list, ", ")
	return result
}
func timesToString(times []int32) string {
	result := "--"
	if len(times) > 0 {
		arr := []string{}
		for _, t := range times {
			h := t / 60
			m := t % 60
			arr = append(arr, fmt.Sprintf("%dh%d", h, m))
		}
		result = strings.Join(arr, ", ")
	}
	return result
}
func PublishPlayRecord3(recordPlay *models.RecordPlay) (int, []byte) {
	media := &base.OPHMediaEdit2{
		MediaId:  *recordPlay.RecId,
		PlayMode: byte(*recordPlay.RecPlayMode),
		PlayTime: *recordPlay.RecPlayTime,
	}
	mediaEditJson, _ := json.Marshal(media)
	for _, id := range *recordPlay.RecReceiptIds {
		var toId int64 = 0
		topic := fmt.Sprintf(groupTopicFormat, id)
		topic2 := fmt.Sprintf(groupTopicFormat2, id)
		if recordPlay.RecReceiptType == int32(base.RECEIPT_TYPE_MCU) {
			toId = id
			topic = fmt.Sprintf(mcuTopicFormat, id)
			topic2 = fmt.Sprintf(mcuTopicFormat2, id)
		}
		//publish to broker
		msg := &base.MqttMessagePublish{
			Id:     toId,
			OpCode: base.OPH_MEDIA_EDIT_2,
			Data:   json.RawMessage(mediaEditJson),
		}
		err := publishMessage(topic, 1, false, msg)
		err = publishMessage(topic2, 1, false, msg)
		if err != nil {
			glog.Errorf("PublishPlayRecord2/publishMessage to %s err: %v", topic, err)
			return http.StatusInternalServerError, nil
		}
		glog.V(8).Infof("PublishPlayRecord2/MediaEdit %s ----> success", topic)
	}
	//Get record info by id in redis
	record := redis.GetRecordInforById(*recordPlay.RecId)
	if record != nil {
		record.RecPlayTime = *recordPlay.RecPlayTime
		record.RecPlayMode = *recordPlay.RecPlayMode
		record.RecReceiptType = recordPlay.RecReceiptType
		record.RecReceiptIds = recordPlay.RecReceiptIds
		//Update to Redis
		err := redis.UpdateRecordPlayInfo(recordPlay)
		if err != nil {
			glog.Error("PublishPlayRecord2/redis.UpdaterecordInfo err: ", err)
		}
	}
	res := &models.RecordRes{
		RecId: *recordPlay.RecId,
	}
	json, _ := json.Marshal(&res)
	return http.StatusOK, json
}
func PublishDeleteRecord2(recordDel *models.RecordDel) (int, []byte) {
	delJson, _ := json.Marshal(*recordDel.RecId)
	for _, id := range *recordDel.RecReceiptIds {
		var toId int64 = 0
		topic := fmt.Sprintf(groupTopicFormat, id)
		topic2 := fmt.Sprintf(groupTopicFormat2, id)
		if *recordDel.RecReceiptType == int32(base.RECEIPT_TYPE_MCU) {
			toId = id
			topic = fmt.Sprintf(mcuTopicFormat, id)
			topic2 = fmt.Sprintf(mcuTopicFormat2, id)
		}
		//publish to broker
		msg := &base.MqttMessagePublish{
			Id:     toId,
			OpCode: base.OPH_MEDIA_DELETE,
			Data:   json.RawMessage(delJson),
		}
		err := publishMessage(topic, 1, false, msg)
		err = publishMessage(topic2, 1, false, msg)
		if err != nil {
			glog.Errorf("PublishDeleteRecord/publishMessage to %s err: %v", topic, err)
			return http.StatusInternalServerError, nil
		}
		glog.V(8).Infof("PublishDeleteRecord/MediaDelete %s ----> success", topic)
	}
	//Delete in redis
	redis.UpdateMcuPhone(1234,"qwwq")
	err := redis.DeleteRecordById(*recordDel.RecId)
	if err != nil {
		glog.Error("PublishDeleteRecord/redis.DeleteRecordById err:", err)
	}
	res := &models.RecordRes{
		RecId: *recordDel.RecId,
	}
	json, _ := json.Marshal(&res)
	return http.StatusOK, json
}
