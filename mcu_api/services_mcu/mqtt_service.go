package services_mcu

import (
	"api_cv/core_api/base"
	"api_cv/core_api/models"
	"api_cv/core_api/redis"
	"api_cv/core_api/settingsMCU"
	"encoding/json"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/golang/glog"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)
var mqttInfo *settingsMCU.MqttInfo = settingsMCU.GetMqttInfo()
//Lấy trạng thái của tất cả client MQTT (MCU Device)
func getMqttStatusClients(path string) ([]models.MqttClientStatus, error) {
	client := &http.Client{}
	url := fmt.Sprintf("http://%s@%s:%d/%s", settingsMCU.GetKeyAuth(), mqttInfo.ServerAddressHTTP, mqttInfo.HttpApiPort, path)


	//url := fmt.Sprintf("http://%s:%d/%s", mqttInfo.ServerAddress, mqttInfo.HttpApiPort, path)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return []models.MqttClientStatus{}, err
	}
	//req.SetBasicAuth(mqttInfo.HttpApiUserName, mqttInfo.HttpApiPassword)
	//fmt.Println(mqttInfo.HttpApiUserName, mqttInfo.HttpApiPassword)
	res, err := client.Do(req)
	//If err returned -> auto close - dont need to close
	if res!=nil{
		defer res.Body.Close()
	}
	if err == nil {
		if res != nil {
			//fmt.Println("IN")
			if res.StatusCode == http.StatusOK {

				data, _ := ioutil.ReadAll(res.Body)
				result := models.MQTTClientList{}
				err := json.Unmarshal(data, &result)

				if err != nil {
					return nil, err
				}
				return result.MQTTClient, nil
			}
			return []models.MqttClientStatus{}, err
		}
	} else {
		//fmt.Println("IN2")
		return []models.MqttClientStatus{}, err
	}
	return []models.MqttClientStatus{}, nil
}
func SetSuperUser() error {
	username := base.LP_MCU_API
	password := base.LP_MCU_API
	if len(mqttInfo.UserName) > 0 {
		username = mqttInfo.UserName
		password = mqttInfo.Password
	}
	return redis.SetAclSuperUser(username, password)
}
func ConnectMqtt() error {
	defer func() {
		if err := recover(); err != nil {
			glog.Error("-------------RECOVER err: ", err)
		}
	}()
	ip, err := resolveHostIp()
	if err != nil {
		return err
	}
	server := fmt.Sprintf("tcp://%s:%d", mqttInfo.ServerAddress, mqttInfo.ServerPort)
	opts := MQTT.NewClientOptions().AddBroker(server)
	opts.SetClientID(fmt.Sprintf("%s@%s", base.LP_MCU_API, ip))
	//username := base.LP_MCU_API
	//password := base.LP_MCU_API
	//if len(mqttInfo.UserName) > 0 {
	//	username = mqttInfo.UserName
	//	password = mqttInfo.Password
	//}
	//
	//opts.SetUsername(username)
	//opts.SetPassword(password)
	opts.SetCleanSession(false)
	opts.SetKeepAlive(60 * time.Second)
	opts.SetAutoReconnect(true)

	mqttClient = MQTT.NewClient(opts)
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	return nil
}
func DisconnectMqtt() {
	if mqttClient != nil {
		mqttClient.Disconnect(250)
	}
}

func resolveHostIp() (string, error) {
	netInterfaceAddresses, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}
	for _, netInterfaceAddress := range netInterfaceAddresses {
		networkIp, ok := netInterfaceAddress.(*net.IPNet)
		if ok && !networkIp.IP.IsLoopback() && networkIp.IP.To4() != nil {
			ip := networkIp.IP.String()
			return ip, nil
		}
	}
	return "", err
}
func publishMessage (topic string, qos byte, retain bool, msg *base.MqttMessagePublish) error{
	payload, err := json.Marshal(msg)
	if err != nil {
		return err
	} else {
		token := mqttClient.Publish(topic, qos, retain, payload)
		return token.Error()
	}
}