package services_mcu

import (
	"api_cv/core_api/base"
	"api_cv/core_api/models"
	"api_cv/core_api/redis"
	"api_cv/core_api/settingsMCU"
	"bytes"
	"encoding/json"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/golang/glog"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)
var tttmInfo *settingsMCU.TTTMInfo = settingsMCU.GetTTTMInfo()
var mcuTopicFormat string = "h/d/%d"
var groupTopicFormat string = "h/g/%d"
var mcuTopicFormat2 string = "h2/d/%d"
var groupTopicFormat2 string = "h2/g/%d"
var mqttClient MQTT.Client
var serverTTTM string = fmt.Sprintf("http://%s:%d", tttmInfo.TTTTMMainFluxAddress, tttmInfo.Port)
func SetCMSGroup2(group *models.McuGroup) (int, []byte) {
	//Kiểm tra xem MCU ID có trong DB Redis hay chưa
	fmt.Println("InCMSGroup")
	ok := redis.ExistsMcuId(*group.McuId)
	//Check xem MQTT Client có đang online hay không
	fmt.Println(*group.McuId)
	onl, _ := checkClientOnline(*group.McuId)
	//Nếu MQTT Client chưa có trong DB hoặc không hoạt động
	//if ok == false || !onl {
	//	return http.StatusNotFound, nil
	//} else {//Nếu không
	fmt.Println("OK Existed",ok)
	fmt.Println("OK online",onl)
	if ok == false || !onl {
		return http.StatusNotFound, nil
	} else {//Nếu không
		fmt.Println("INN")
		fmt.Println(*group.McuId)
		//TOTO: delete and update acl in redis
		err := redis.DeleteAclMcuTopic(*group.McuId)
		if err != nil {
			glog.Error("SetCMSGroup/redis.DeleteAclMcuTopic err: ", err)
			return http.StatusInternalServerError, nil
		}
		err = redis.SetAclMcuTopic(*group.McuId, *group.CmsGroupIdList)
		if err != nil {
			glog.Error("SetCMSGroup/redis.SetAclMcuTopic err: ", err)
			return http.StatusInternalServerError, nil
		}

		//TODO: publish to broker
		groupJson, _ := json.Marshal(group.CmsGroupIdList)
		msg := &base.MqttMessagePublish{
			Id:     *group.McuId,
			OpCode: base.OPH_GROUP_UPDATE,
			Data:   json.RawMessage(groupJson),
		}
		topic := fmt.Sprintf(mcuTopicFormat, *group.McuId)
		topic2 := fmt.Sprintf(mcuTopicFormat2, *group.McuId)

		err = publishMessage(topic, 1, false, msg)
		err = publishMessage(topic2, 1, false, msg)
		if err != nil {
			glog.Errorf("SetCMSGroup/publishMessage to %s err: %v", topic, err)
			return http.StatusInternalServerError, nil
		}
		//Update to redis
		err = redis.UpdateMcuGroupList(*group.McuId, *group.CmsGroupIdList)
		if err != nil {
			glog.Errorf("SetCMSGroup/redis.UpdateMcuGroupList err: ", err)
		}
		glog.V(8).Infof("SetCMSGroup to %s ----> success", topic)
		res := &models.McuRes{
			McuId: *group.McuId,
		}
		jsonRes, _ := json.Marshal(&res)
		body := &models.McuIDReqTTTM{
			McuId:*group.McuId,
		}
		client := &http.Client{}
		jsonMCU, _ := json.Marshal(body)
		fmt.Println(string(jsonMCU))
		req, err := http.NewRequest(http.MethodPost, serverTTTM+"/api/v1/tttm/set-group", bytes.NewBuffer(jsonMCU))
		if err !=nil{
			fmt.Println("Fail request Set group MCU TTTM")
			return 1,nil
		}
		fmt.Println(serverTTTM+"/api/v1/tttm/set-group")
		req.Header.Set("Content-Type","application/json")
		req.Header.Set("Accept","application/json")
		resTTTM, err := client.Do(req)
		data, _ := ioutil.ReadAll(resTTTM.Body)
		responseMSG := new(models.ResponseMSGTTTM)
		json.Unmarshal(data,&responseMSG)
		s,_ := (json.Marshal(responseMSG))
		fmt.Println(string(s))
		if responseMSG.Status == 1{
			return http.StatusOK, jsonRes
		} else {
			msgFail := []byte(responseMSG.Message)
			switch responseMSG.Code {
			case "SET_GROUP_ERROR_0001":
				return 502,msgFail
			case "SET_GROUP_ERROR_0002":
				return 405,msgFail
			case "SYSTEM_ERROR":
				return 501,msgFail
			default:
				return 501,msgFail
			}
		}
	}
}
//Kiểm tra Xem Client có đang online hay không
func checkClientOnline(id int64) (bool, error) {
	fmt.Println("In check client")
	conns, err := getMqttStatusClients("api/v1/session/show")
	if err != nil {
		fmt.Println("Get client online fail")
		glog.Error("checkMqttStatusClients/getMqttStatusClients err: ", err)
		return false, err
	}
	fmt.Println("len",len(conns))
	for _, conn := range conns {

		mcuId, err := strconv.ParseInt(conn.ClientId, 10, 64)
		fmt.Println("mcuId Online",mcuId)
		if err != nil {
			glog.Error("checkMqttStatusClients err: ", err)
			continue
		}
		if id == mcuId && conn.IsOnline == true {
			return true, nil
		}
	}
	return false, nil
}
func VerifyVcode(vcode *models.McuVCode) (int, []byte) {
	fmt.Println("Haizz",vcode.McuId)
	//TODO: get data from redis
	mcu := redis.GetMcuInfo(*vcode.McuId)
	onl, _ := checkClientOnline(*vcode.McuId)
	//onl := true
	if mcu == nil || !onl {
		if mcu == nil {
			glog.Infof("VerifyVcode/%d Not Found", *vcode.McuId)
		}
		if !onl {
			glog.Infof("VerifyVcode/%d offline", *vcode.McuId)
		}
		return http.StatusNotFound, nil
	} else {
		fmt.Println(*vcode.VCode,mcu.VCode)
		if strings.Compare(*vcode.VCode,mcu.VCode) ==0 {
			fmt.Println("INssdasd1111")
			res := &models.McuRes{
				McuId: *vcode.McuId,
			}
			jsonRS, _ := json.Marshal(&res)
			body := &models.MCUVerifyTTTM{
				McuId:     *vcode.McuId,
				VCode:     *vcode.VCode,
				Name:      "Mira",
				Address:   "",
				Lat:       "0",
				Lon:       "0",
				SimNumber: mcu.PhoneNumber,
				SimSerial: "",
			}
			fmt.Println("INssdasd")
			client := &http.Client{}
			jsonMCU, _ := json.Marshal(body)
			fmt.Println(string(jsonMCU))
			req, err := http.NewRequest(http.MethodPost, serverTTTM+"/api/v1/tttm/verify-mcu", bytes.NewBuffer(jsonMCU))
			if err !=nil{
				fmt.Println("Fail request Verify MCU TTTM")
				return 1,nil
			}
			fmt.Println(serverTTTM+"/api/v1/tttm/verify-mcu")
			req.Header.Set("Content-Type","application/json")
			fmt.Println("____fail1")
			//req.Header.Set("Accept","application/json")
			resTTTM, err := client.Do(req)
			fmt.Println("____fail2")

			defer resTTTM.Body.Close()
			fmt.Println("____fail3")
			if err == nil {
				if res != nil {
					if resTTTM.StatusCode == 200 {
						fmt.Println("____fail4")
						data, _ := ioutil.ReadAll(resTTTM.Body)
						responseMSG := new(models.ResponseMSGTTTM)
						json.Unmarshal(data,&responseMSG)
						s,_ := (json.Marshal(responseMSG))
						fmt.Println(string(s))
						if responseMSG.Status == 1{
							//client := &http.Client{}
							fmt.Println("IN")
							path:="api/v1/session/disconnect?client-id"
							url := fmt.Sprintf("http://%s@%s:%d/%s=%d&--cleanup", settingsMCU.GetKeyAuth(), mqttInfo.ServerAddressHTTP, mqttInfo.HttpApiPort, path,vcode.McuId)
							fmt.Println(url)
							req, err := http.NewRequest(http.MethodGet, url, nil)
							if err !=nil{
								fmt.Println("fail")
								glog.Error("Fail to request to API disconnect VMQ")
								return 500,nil
							}
							res, err := client.Do(req)
							//If err returned -> auto close - dont need to close
							if res!=nil{
								defer res.Body.Close()
							}
							if err == nil {
								if res != nil {
									//fmt.Println("IN")
									if res.StatusCode == http.StatusOK {
										fmt.Println("OK")
										data, _ := ioutil.ReadAll(res.Body)
										result := models.DisconnectClientMQTT{}
										err := json.Unmarshal(data, &result)
										if err != nil {
											return 500, []byte(err.Error())
										}
										if result.Text == "Done"{
											fmt.Println("DONE")
											return http.StatusOK, jsonRS
										}
									}
									return 500, []byte(err.Error())
								}
							} else {
								//fmt.Println("IN2")
								return 500, []byte(err.Error())
							}
						} else {
							//switchString:= responseMSG.Message
							msgFail := []byte(responseMSG.Message)
							switch responseMSG.Code {
							case "MCU_VERIFY_ERROR_0001":
								//fmt.Println(http.StatusUnauthorized,msgFail)
								return 402,msgFail
							case "MCU_VERIFY_ERROR_0002":
								//fmt.Println(http.StatusNotFound,msgFail)
								return 405,msgFail
							case "MCU_VERIFY_ERROR_0003":
								///fmt.Println(http.StatusNotFound,msgFail)
								return 406,msgFail
							case "MCU_VERIFY_ERROR_0004":
								//fmt.Println(http.StatusInternalServerError,msgFail)
								return 502,msgFail
							case "AUTH_ERROR":
								return 402,msgFail
							case "SYSTEM_ERROR":
								return 501,msgFail
							}
						}
					}
				}
			} else {
				return http.StatusNotFound,nil
			}
			return http.StatusOK, jsonRS
		} else {
			return http.StatusUnauthorized, nil
		}
	}
}
func SetVolume2(volume *models.McuVolume) (int, []byte) {
	fmt.Println("SetVolumn2")
	ok := redis.ExistsMcuId(*volume.McuId)
	onl, _ := checkClientOnline(*volume.McuId)
	if ok == false || !onl {
		fmt.Println("fail")
		return http.StatusNotFound, nil
	} else {
		//TODO: publish to broker
		volumeJson, _ := json.Marshal(volume.Volume)
		//fmt.Println(volumeJson)
		msg := &base.MqttMessagePublish{
			Id:     *volume.McuId,
			OpCode: base.OPH_MEDIA_VOLUME,
			Data:   json.RawMessage(volumeJson),
		}
		fmt.Println(*volume.McuId)
		topic := fmt.Sprintf(mcuTopicFormat, *volume.McuId)
		topic2 := fmt.Sprintf(mcuTopicFormat2, *volume.McuId)
		err := publishMessage(topic, 1, false, msg)
		err = publishMessage(topic2, 1, false, msg)
		if err != nil {
			glog.Errorf("SetVolume/publishMessage to %s err: %v", topic, err)
			return http.StatusInternalServerError, nil
		}
		glog.V(8).Infof("SetVolume to %s ----> success", topic)
		//Update to redis
		err = redis.UpdateMcuVolume(*volume.McuId, *volume.Volume)
		if err != nil {
			glog.Error("SetVolume/redis.UpdateMcuVolume err: ", err)
		}
		res := &models.McuRes{
			McuId: *volume.McuId,
		}
		json, _ := json.Marshal(&res)
		return http.StatusOK, json
	}
}
func RebootMCU2(reboot *models.McuReboot) (int, []byte) {
	for _, id := range *reboot.ReceiptIds {
		fmt.Println(id)
		var toId int64 = 0
		topic := fmt.Sprintf(groupTopicFormat, id)
		topic2 := fmt.Sprintf(groupTopicFormat2, id)
		if *reboot.ReceiptType == int32(base.RECEIPT_TYPE_MCU) {
			toId = id
			topic = fmt.Sprintf(mcuTopicFormat, id)
			topic2 = fmt.Sprintf(mcuTopicFormat2, id)
		}
		//publish to broker
		msg := &base.MqttMessagePublish{
			Id:     toId,
			OpCode: base.OPH_REBOOT,
			Data:   nil,
		}
		err := publishMessage(topic, 0, false, msg)
		err = publishMessage(topic2, 0, false, msg)
		if err != nil {
			glog.Errorf("PublishRebootMcu/publishMessage to %s err: %v", topic, err)
			return http.StatusInternalServerError, nil
		}
		glog.V(8).Infof("PublishRebootMcu to %s ----> success", topic)
	}
	return http.StatusOK, nil
}
func LoadData(id int64) (int, []byte) {
	//TODO: get data from redis
	mcu := redis.GetMcuInfo(id)
	if mcu == nil {
		fmt.Println("Khong thay mcu")
		return http.StatusNotFound, nil
	} else {
		fmt.Println("Thay MCU roi")
		cameras := []models.McuCamera{}
		for _, cam := range mcu.CameraList {
			cameras = append(cameras, models.McuCamera{
				McuCameraId:       cam.CameraId,
				McuCameraName:     cam.CameraName,
				McuCameraLocalIp:  cam.CameraLocalIp,
				McuCameraTypeId:   cam.CameraTypeId,
				McuCameraHttpPort: cam.CameraHttpPort,
				McuCameraRtspPort: cam.CameraRtspPort,
				McuCameraUsername: cam.CameraUsername,
				McuCameraPassword: cam.CameraPassword,
			})
		}
		sensors := []models.Sensor{}

		for _, sensor := range mcu.SensorList {
			enable := true
			if sensor.Enable != 1 {
				enable = false
			}
			sensors = append(sensors, models.Sensor{
				McuSensorId:         sensor.SensorId,
				McuSensorType:       int32(sensor.Type),
				McuSensorName:       sensor.Name,
				McuSensorEnable:     enable,
				McuSensorThresholds: sensor.Thresholds,
				McuSensorState:      int32(sensor.State),
				McuSensorData:       int64(sensor.Value),
				McuSensorUpdateTime: sensor.CreatedTime,
			})
		}
		events := []models.Event{}
		for _, alarm := range mcu.AlarmList {
			events = append(events, models.Event{
				McuEventId:               alarm.EventId,
				McuEventName:             alarm.Name,
				McuEventType:             int32(alarm.EventType),
				McuEventSensorId:         alarm.SensorId,
				McuEventSensorState:      int32(alarm.SensorState),
				McuEventActiveMode:       int32(alarm.Mode),
				McuEventActiveAutoTime:   int32(alarm.ActiveTime),
				McuEventInactiveAutoTime: int32(alarm.InactiveTime),
				McuEventActiveAutoDays:   int32(alarm.AutoDays),
				McuEventPlayFile:         int64(alarm.PlayFile),
				McuEventUpdateTime:       alarm.OccurTime,
			})
		}
		mcuData := &models.Mcu{
			McuId:          id,
			McuTelNumber:   mcu.PhoneNumber,
			McuVolume:      int32(mcu.Volume),
			McuIp:          mcu.WanIP,
			CmsGroupIdList: mcu.GroupList,
			McuCameraList:  cameras,
			McuSensorList:  sensors,
			McuEventList:   events,
		}
		if mcuData != nil {
			json, err := json.Marshal(&mcuData)
			if err == nil {
				return http.StatusOK, json
			} else {
				return http.StatusInternalServerError, nil
			}
		} else {
			return http.StatusNotFound, nil
		}
	}
}
func Delete(id int64) int {
	mcu := redis.GetMcuInfo(id)
	if mcu == nil {
		return http.StatusNotFound
	} else {
		if mcu.VCode == "" {
			return http.StatusNotFound
		}
		err := redis.DeleteMcuById(id)
		if err != nil {
			return http.StatusInternalServerError
		} else {
			body := &models.McuIDReqTTTM{
				McuId:id,
			}
			client := &http.Client{}
			jsonMCU, _ := json.Marshal(body)
			fmt.Println(string(jsonMCU))
			req, err := http.NewRequest(http.MethodPost, serverTTTM+"/api/v1/tttm/delete-mcu", bytes.NewBuffer(jsonMCU))
			if err !=nil{
				fmt.Println("Fail request Set group MCU TTTM")
				return http.StatusNotAcceptable
			}
			fmt.Println(serverTTTM+"/api/v1/tttm/delete-mcu")
			req.Header.Set("Content-Type","application/json")
			//req.Header.Set("Accept","application/json")
			resTTTM, err := client.Do(req)
			data, _ := ioutil.ReadAll(resTTTM.Body)
			responseMSG := new(models.ResponseMSGTTTM)
			json.Unmarshal(data,&responseMSG)
			fmt.Println("IN")
			s,_ := (json.Marshal(responseMSG))
			fmt.Println(string(s))
			if responseMSG.Status == 1{
				fmt.Println("OK")
				return http.StatusOK
			} else {
				//msgFail := []byte(responseMSG.Message)
				switch responseMSG.Code {
				case "MCU_DELETE_ERROR_0001":
					return 405
				case "MCU_DELETE_ERROR_0002":
					return 406
				case "MCU_DELETE_ERROR_0003":
					return 502
				case "AUTH_ERROR":
					return 503
				case "SYSTEM_ERROR":
					return 501
				default:
					return 501
				}
			}
		}
	}
}
