package controllers

import (
	"api_cv/mcu_api/services_mcu"
	"encoding/json"
	"net/http"
)

func PutClientids(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	clientIds := []string{}
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&clientIds)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if len(clientIds) == 0 {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			status, res := services_mcu.SetClientIds(clientIds)
			w.WriteHeader(status)
			if status == http.StatusOK {
				w.Write(res)
			}
		}
	}
}
