package controllers

import (
	"api_cv/core_api/models"
	"api_cv/mcu_api/services_mcu"
	"encoding/json"
	"fmt"
	"net/http"
)

func AddRecord2(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	fmt.Println("IN add record")
	record := new(models.Record)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(record)
	//fmt.Println(record)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || record.RecId == nil || record.RecSummary == nil || record.RecAudio ==nil || record.RecReceiptIds == nil{
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if len(*record.RecReceiptIds) == 0 {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			//status, res := services.PublishNewRecord2(record)
			status, res := services_mcu.PublishNewRecord3(record)
			w.WriteHeader(status)
			if status == http.StatusOK {
				w.Write(res)
			}
		}
	}
}
func EditRecord2(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	recordPlay := new(models.RecordPlay)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(recordPlay)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || recordPlay.RecId == nil || recordPlay.RecPlayTime == nil || recordPlay.RecPlayMode == nil || recordPlay.RecReceiptIds ==nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		//status, res := services.PublishPlayRecord2(recordPlay)
		status, res := services_mcu.PublishPlayRecord3(recordPlay)
		w.WriteHeader(status)
		if status == http.StatusOK {
			w.Write(res)
		}
	}
}

func DeleteRecord(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	recordDel := new(models.RecordDel)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&recordDel)
	a,s:=json.Marshal(recordDel)
	fmt.Println(string(a))
	fmt.Println(s)
	//fmt.Println(recordDel)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || recordDel.RecReceiptIds == nil || recordDel.RecId == nil || recordDel.RecReceiptType == nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if len(*recordDel.RecReceiptIds) == 0 {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			//status, res := services.PublishDeleteRecord(recordDel)
			status, res := services_mcu.PublishDeleteRecord2(recordDel)
			w.WriteHeader(status)
			if status == http.StatusOK {
				w.Write(res)
			}
		}
	}
}