package controllers

import (
	"api_cv/core_api/models"
	"api_cv/mcu_api/services_mcu"
	"encoding/json"
	"fmt"
	_ "github.com/golang/glog"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)
//thêm thiết bị
func SetCMSGroup(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	//fmt.Println("In SetCMSGroup")
	//var tmpgroup models.McuGroup
	//body, _ := ioutil.ReadAll(r.Body)
	//json.Unmarshal([]byte(body),&tmpgroup)
	//fmt.Println(tmpgroup.McuId)
	groups := new(models.McuGroup)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	err := decoder.Decode(&groups)
	//fmt.Println(groups)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || groups.McuId == nil || groups.CmsGroupIdList ==nil {
		//fmt.Println("IN")
		w.WriteHeader(http.StatusBadRequest)
	} else {
		//status, res := services.SetCMSGroup(groups)
		status, res := services_mcu.SetCMSGroup2(groups)
		w.WriteHeader(status)
		if status == http.StatusOK {
			w.Write(res)
		}
	}
}
func LoadDataMCU(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		status, res := services_mcu.LoadData(id)
		w.WriteHeader(status)
		if status == http.StatusOK {
			w.Write(res)
		}
	}
}

func VerifyMCU(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	fmt.Println("In Verify")
	vcode := new(models.McuVCode)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(vcode)
	fmt.Println(vcode)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || vcode.McuId == nil || vcode.VCode == nil{
		w.WriteHeader(http.StatusBadRequest)
	} else {
		status, res := services_mcu.VerifyVcode(vcode)
		w.WriteHeader(status)
		if status == http.StatusOK {
			w.Write(res)
		}
	}
}

func SetVolume(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	volume := new(models.McuVolume)
	//fmt.Println("In Set Volumn")
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&volume)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil ||volume.McuId==nil || volume.Volume == nil || *volume.Volume < 0 || *volume.Volume > 100 {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		//status, res := services.SetVolume(volume)
		status, res := services_mcu.SetVolume2(volume)
		w.WriteHeader(status)
		if status == http.StatusOK {
			w.Write(res)
		}
	}
}
func RebootMCU(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	reboot := new(models.McuReboot)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(reboot)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil || reboot.ReceiptIds == nil || reboot.ReceiptType == nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if len(*reboot.ReceiptIds) == 0 {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			//status, _ := services.RebootMCU(reboot)
			status, _ := services_mcu.RebootMCU2(reboot)
			w.WriteHeader(status)
		}
	}
}
func DeleteMCU(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		status := services_mcu.Delete(id)
		fmt.Println(status)
		w.WriteHeader(status)
	}
}
