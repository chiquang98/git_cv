package controllers

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

func Hello(w http.ResponseWriter, r *http.Request, n http.HandlerFunc) {
	//fmt.Println("In Hello")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(time.Now().String()))
	vars := mux.Vars(r)
	key := vars["hello"]

	fmt.Fprintf(w, "Key: " + key)
}
