package routers

import (
	"api_cv/core_api/settingsCMS"
	"github.com/gorilla/mux"
	auth "github.com/nabeken/negroni-auth"
)

var authBasic = auth.Basic(settingsCMS.GetUserNameAuth(), settingsCMS.GetPasswordAuth())

func InitRoutes() *mux.Router {
	router := mux.NewRouter()
	router = SetMcuRouter(router)
	router = SetRecordRouter(router)
	router = SetHelloRouter(router)
	router = SetClientIdRouter(router)
	return router
}
