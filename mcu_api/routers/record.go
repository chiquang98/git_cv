package routers

import (
	"api_cv/mcu_api/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetRecordRouter(router *mux.Router) *mux.Router {
	router.Handle("/mcu/v2/records",
		negroni.New(
			negroni.HandlerFunc(controllers.AddRecord2),
		)).Methods("POST")
	router.Handle("/mcu/v2/records",
		negroni.New(
			negroni.HandlerFunc(controllers.EditRecord2),
		)).Methods("PUT")
	router.Handle("/mcu/v1/records/{id}",
		negroni.New(
			negroni.HandlerFunc(controllers.DeleteRecord),
		)).Methods("DELETE")
	return router
}