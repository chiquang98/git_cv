package routers
import (
	"api_cv/mcu_api/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)
func SetHelloRouter(router *mux.Router) *mux.Router {

	router.Handle("/mcu/v1/{hello}",
		negroni.New(
			negroni.HandlerFunc(controllers.Hello),
		)).Methods("GET")
	return router
}
