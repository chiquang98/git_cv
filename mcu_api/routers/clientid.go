package routers

import (
	"api_cv/mcu_api/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetClientIdRouter(router *mux.Router) *mux.Router {
	router.Handle("/mcu/v1/set-clientids",
		negroni.New(
			negroni.HandlerFunc(authBasic),
			negroni.HandlerFunc(controllers.PutClientids),
		)).Methods("PUT")
	return router
}

