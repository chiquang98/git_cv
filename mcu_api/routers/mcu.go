package routers

import (
	"api_cv/mcu_api/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetMcuRouter(router *mux.Router) *mux.Router {
	router.Handle("/mcu/v1/load-data-mcu/{id}",
		negroni.New(
			negroni.HandlerFunc(controllers.LoadDataMCU),
		)).Methods("GET")
	router.Handle("/mcu/v1/verify-mcu",
		negroni.New(
			negroni.HandlerFunc(controllers.VerifyMCU),
		)).Methods("POST")
	router.Handle("/mcu/v1/delete-mcu/{id}",
		negroni.New(
			negroni.HandlerFunc(controllers.DeleteMCU),
		)).Methods("DELETE")
	router.Handle("/mcu/v1/set-cms-group-mcu",
		negroni.New(
			negroni.HandlerFunc(controllers.SetCMSGroup),
		)).Methods("PUT")
	router.Handle("/mcu/v1/set-volume-mcu",
		negroni.New(
			negroni.HandlerFunc(controllers.SetVolume),
		)).Methods("PUT")
	router.Handle("/mcu/v1/reboot-mcu",
		negroni.New(
			negroni.HandlerFunc(controllers.RebootMCU),
		)).Methods("POST")
	return router
}