package main

import (
	"api_cv/core_api/redis"
	"api_cv/core_api/settingsCMS"
	"api_cv/lp-cms-client/services"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"os"
	"path/filepath"
	"time"
)

const version string = "dev_1_1"

func init() {
	//glog
	//create logs folder
	absPath, _ := filepath.Abs(filepath.Join("lp-cms-client","logs"))
	err := os.Mkdir(absPath, 0777)
	if err !=nil{
		fmt.Println("Folder Logs CMS already exist, Continue writing log to this folder")
	}
	flag.Lookup("stderrthreshold").Value.Set("[INFO|WARN|FATAL]")
	flag.Lookup("logtostderr").Value.Set("false")
	flag.Lookup("alsologtostderr").Value.Set("true")
	flag.Lookup("log_dir").Value.Set(absPath)
	glog.MaxSize = 1024 * 1024 * settingsCMS.GetGlogConfig().MaxSize
	flag.Lookup("v").Value.Set(fmt.Sprintf("%d", settingsCMS.GetGlogConfig().V))
	flag.Parse()

}

func main() {
	flag.Parse()
	defer func() {
		if err := recover(); err != nil {
			glog.Error("-------------RECOVER err: ", err)
		}
	}()
	glog.Infof("START LP-CMS-CLIENT %v, at: %v", version, time.Now())
	glog.Info("Init Redis database...")
	redis.Init1(settingsCMS.GetRedisInfo().Host1,settingsCMS.GetRedisInfo().Host2,settingsCMS.GetRedisInfo().Host3)
	if !redis.Ping() {
		glog.Info("redis ping error")
		os.Exit(0)
	} else {
		glog.Info("OK")
	}
	//init mqtt
	err := services.SetSuperUser()
	if err == nil {
		glog.Info("Connecting to MQTT broker... ")
		err := services.ConnectMqtt()
		if err != nil {
			glog.Info("Failed, exit")
			os.Exit(1)
		} else {
			glog.Info("OK")
		}
	}
	//generic := &base.OPUGeneric{}
	//generic.VCode = "VcodeTest"
	//redis.UpdateMcuInfo(0,generic)
	ticker := time.NewTicker(60 * time.Second)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				services.CheckMqttStatusClients()
			}
		}
	}()

	stats()
}

func stats() {
	var s string
	for {
		fmt.Scanln(&s)
		switch s {
		case "q":
			services.DisconnectMqtt()
			glog.Info("exited")
			os.Exit(0)
		case "t":
			services.Test()
		}
	}
}
